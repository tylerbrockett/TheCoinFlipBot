from utils.inbox_handler import InboxHandler
from utils.reddit_handler import RedditHandler
from utils.sleep_handler import SleepHandler
from utils.env import env, BOT_USERNAME

class Bot:
    def __init__(self):
        self.run = True
        self.reddit = RedditHandler()

    def start(self):
        print('Starting bot as ' + env(BOT_USERNAME) + '...')
        while True:
            try:
                if self.run:
                    InboxHandler.read_inbox(self.reddit)
                SleepHandler.sleep(20)
            except KeyboardInterrupt:
                print('Keyboard Interrupt - Bot killed')
                exit()
            except Exception as e:
                try:
                    self.reddit.reset()
                except Exception as e:
                    print('Failed to restart bot. Trying again soon.')
            SleepHandler.sleep(15)

bot = Bot()
bot.start()
