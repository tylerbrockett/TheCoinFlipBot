from .sleep_handler import SleepHandler
from utils.env import env, BOT_USERNAME, DEV_USERNAME
import traceback
import random

GITHUB_HOME = 'https://github.com/tylerbrockett/TheCoinFlipBot'
GITHUB_README = 'https://github.com/tylerbrockett/TheCoinFlipBot/blob/master/README.md'


class InboxHandler:

    @staticmethod
    def format_subject(s):
        while len(s) >= 3 and s[:3].lower() == 're:':
            s = s[3:]
        while len(s) > 0 and s[0] == ' ':
            s = s[1:]
        return s

    @staticmethod
    def compose_greeting(username):
        return 'Hi /u/' + username + ',\t \n'

    @staticmethod
    def compose_salutation():
        return '\n\t \n\t \n-/u/' + env(BOT_USERNAME) + ' ([code](' + GITHUB_HOME + '))'

    @staticmethod
    def compose_username_mention_reply(username, result):
        result = InboxHandler.compose_greeting(username) + '\t \n' + \
                'You have just flipped a coin, and it landed on...\t \n' + \
                '>!' + result + '!<\t \n' + \
                InboxHandler.compose_salutation()
        return result

    @staticmethod
    def compose_post_reply_forward(developer_username, username, body):
        result = InboxHandler.compose_greeting(developer_username) + \
                'Someone has responded to a post by the bot! the comment is quoted below:\n\n' + \
                'USERNAME: ' + username + '\nBODY:\n' + body
        return result

    @staticmethod
    def reply(message, response):
        print('Replying to message: ' + message.id)
        message.reply(response[0: min(len(response), 10000)])

    @staticmethod
    def handle_message_from_reddit(reddit, message):
        print('Message from reddit')
        reddit.send_message(env(DEV_USERNAME), 'FWD: ' + message.subject, message.body)
        message.mark_read()

    @staticmethod
    def handle_username_mention_message(reddit, message):
        print('Username mention message')
        try:
            result = 'Heads' if bool(random.getrandbits(1)) else 'Tails'
            InboxHandler.reply(message, InboxHandler.compose_username_mention_reply(str(message.author), result))
            message.mark_read()

        except Exception as e:  # Figure out more specific exception thrown (praw.exceptions.APIException?)
            print(str(e))
            print('Handled RateLimitExceeded praw error - Commenting too frequently')

    @staticmethod
    def read_inbox(reddit):
        print('Reading inbox...')
        unread = []
        try:
            unread = reddit.get_unread()
        except Exception as e:
            unread = []

        num_messages = 0
        for message in unread:
            num_messages += 1
            username = str(message.author).lower()
            subject = InboxHandler.format_subject(str(message.subject).lower())
            body = str(message.body).lower()
            try:
                if username == 'reddit':
                    InboxHandler.handle_message_from_reddit(reddit, message)
                elif subject == 'username mention' or (subject == 'comment reply' and env(BOT_USERNAME).lower() in body):
                    InboxHandler.handle_username_mention_message(reddit, message)
                elif subject == 'post reply':
                    InboxHandler.handle_post_reply_message(reddit, message)
            except Exception as e:
                print(traceback.format_exc())
            SleepHandler.sleep(2)
        print(str(num_messages) + ' unread messages handled')
