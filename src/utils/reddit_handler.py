import praw
from utils.env import (
    env,
    CLIENT_ID,
    CLIENT_SECRET,
    BOT_USERNAME,
    BOT_PASSWORD,
    USER_AGENT,
)

class RedditHandler:

    def __init__(self):
        self.reddit = self.connect()

    def connect(self):
        reddit = praw.Reddit(
            client_id       = env(CLIENT_ID),
            client_secret   = env(CLIENT_SECRET),
            username        = env(BOT_USERNAME),
            password        = env(BOT_PASSWORD),
            user_agent      = env(USER_AGENT)
        )
        return reddit

    def disconnect(self):
        self.reddit = None

    def reset(self):
        self.disconnect()
        self.reddit = self.connect()

    def get_unread(self):
        ret = []
        unread = self.reddit.inbox.unread(limit=None)
        for message in unread:
            ret.append(message)
        ret.reverse()
        return ret

    def send_message(self, redditor, subject, body):
        print('Sending message to: ' + redditor + ', subject: ' + subject)
        self.reddit.redditor(redditor).message(subject, body)
